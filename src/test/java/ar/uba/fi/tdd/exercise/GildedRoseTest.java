package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {
	@Test
	public void TestNormalGoodQualityDegradesWhenUpdated() {
		Good[] Goods = new Good[] {
			new NormalGood("name", 2, 2)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(1).isEqualTo( app.getGood("name").getQuality() );
	}

	@Test
	public void TestNormalGoodSelllInDecreasesWhenUpdated() {
		Good[] Goods = new Good[] {
			new NormalGood("name", 1, 2)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("name").getSellIn() );
	}

	@Test
	public void TestNormalGoodQualityDegradesTwiceAsFastAfterSellDate() {
		Good[] Goods = new Good[] {
			new NormalGood("name", 0, 2)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("name").getQuality() );
	}

	@Test
	public void TestAgedBrieIncreasesInQualityTheOlderItGets() {
		Good[] Goods = new Good[] {
			new AgedBrie(2, 2)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(3).isEqualTo( app.getGood("Aged Brie").getQuality() );
	}

	@Test
	public void
	TestAgedBrieIncreaseInQualityTwiceAsFastTheOlderItGetsAfterDate() {
		Good[] Goods = new Good[] {
				new AgedBrie(1, 2)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(4).isEqualTo( app.getGood("Aged Brie").getQuality() );
	}

	@Test
	public void
	TestAgedBrieIncreaseInQualityTwiceAsFastTheOlderItGetsAfterDate_2() {
		Good[] Goods = new Good[] {
			new AgedBrie(0, 2)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(4).isEqualTo( app.getGood("Aged Brie").getQuality() );
	}

	@Test
	public void TestSulfurasDoesNotGetQualityUpdated() {
		Good[] goods = new Good[] {
			new Sulfuras()
		};
		GildedRose app = new GildedRose(goods);
		app.dailyUpdate();
		assertThat(80).isEqualTo( app.getGood("Sulfuras").getQuality() );
	}

	@Test
	public void TestQualityCannotBeHigherThan50ForNormalItems() {
		try {
			new NormalGood("name", 0, 51);
		}

		catch (InvalidQualityException e) {
			assertThat(true).isEqualTo(true);
		}

		assertThat(false).isEqualTo(false);
	}

	@Test
	public void TestQualityOfAnItemIsNeverNegative() {
		try {
			new NormalGood("name", 0, -1);
		}

		catch (InvalidQualityException e) {
			assertThat(true).isEqualTo(true);
		}

		assertThat(false).isEqualTo(false);
	}

	@Test
	public void TestSellInIsCannotBeNegative() {
		try {
			new NormalGood("name", -1, 0);
		}

		catch (Exception e) {
			assertThat(true).isEqualTo(true);
		}

		assertThat(false).isEqualTo(false);
	}

	@Test
	public void TestQualityOfAnItemIsNeverNegativeWhenUpdated() {
		Good[] items = new Good[] {
			new NormalGood("name", 0, 0)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("name").getQuality() );
	}

	@Test
	public void TestSellInIsNeverNegativeWhenUpdated() {
		Good[] items = new Good[] {
			new NormalGood("name", 0, 0)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("name").getQuality() );
	}

	@Test
	public void TestBackstagePassIncreasesInQualityWithTime() {
		Good[] items = new Good[] {
				new BackstagePass(20, 1)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(2).isEqualTo( app.getGood("Backstage Pass").getQuality() );
	}

	@Test
	public void TestBackstagePassIncreasesQualityBy3With5DaysOrLessLeft() {
		Good[] items = new Good[] {
				new BackstagePass(5, 2)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(5).isEqualTo( app.getGood("Backstage Pass").getQuality() );
	}

	@Test
	public void TestBackstagePassIncreasesQualityBy3With5DaysOrLessLeft_2() {
		Good[] items = new Good[] {
				new BackstagePass(3, 2)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(5).isEqualTo( app.getGood("Backstage Pass").getQuality() );
	}

	@Test
	public void
		TestBackstagePassIncreasesQualityBy2With10to6DaysOrLessLeft() {
		Good[] items = new Good[] {
				new BackstagePass(10, 4)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(6).isEqualTo( app.getGood("Backstage Pass").getQuality() );
	}

	@Test
	public void
		TestBackstagePassIncreasesQualityBy2With10to6DaysOrLessLeft_2() {
		Good[] items = new Good[] {
				new BackstagePass(8, 4)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(6).isEqualTo( app.getGood("Backstage Pass").getQuality() );
	}

	@Test
	public void
		TestBackstagePassQualityDropsTo0AfterSellDateExpired() {
		Good[] items = new Good[] {
				new BackstagePass(1, 4)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("Backstage Pass").getQuality() );
	}

	@Test
	public void
		TestBackstagePassCannotHaveQualityIfThereAreNoDaysToSell() {
		try {
			new BackstagePass(0, 4);
		}

		catch (Exception e) {
			assertThat(true).isEqualTo(true);
		}

		assertThat(false).isEqualTo(false);
	}

	@Test
	public void TestConjuredDecreasesInQualityTwiceAsFasterAsNormal() {
		Good[] items = new Good[] {
				new Conjured(3, 2)
		};
		GildedRose app = new GildedRose(items);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("Conjured").getQuality() );
	}

	@Test
	public void TestConjuredQualityDegradesTwiceAsFastAfterSellDate() {
		Good[] Goods = new Good[] {
				new Conjured(1, 5)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(1).isEqualTo( app.getGood("Conjured").getQuality() );
	}

	@Test
	public void TestConjuredQualityCannotBeNegativeWhenUpdated() {
		Good[] Goods = new Good[] {
				new Conjured(0, 3)
		};
		GildedRose app = new GildedRose(Goods);
		app.dailyUpdate();
		assertThat(0).isEqualTo( app.getGood("Conjured").getQuality() );
	}
}

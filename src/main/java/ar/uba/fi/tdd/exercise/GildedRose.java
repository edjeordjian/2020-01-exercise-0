package ar.uba.fi.tdd.exercise;

import java.util.*;

class GildedRose {
    private List<Good> goods;

    public GildedRose(Good[] goods) {
        this.goods = new ArrayList<>();
        Collections.addAll(this.goods, goods);
    }

    public void dailyUpdate() {
        for (Good g: goods) {
            g.update();
        }
    }

    public Good getGood(String s) {
        for (Good g : this.goods) {
            if ( g.toString().equals(s) ) {
                return g;
            }
        }

        return null;
    }
}

package ar.uba.fi.tdd.exercise;

public abstract class LegendaryGood extends Good {
    public static final int LEGENDARY_QUALITY = 80;
    public static final int LEGENDARY_DAYS_TO_SELL = 0;

    public LegendaryGood(String name) {
        super(name, LEGENDARY_DAYS_TO_SELL, LEGENDARY_QUALITY);
    }

    @Override
    public int sellInUpdate() {
        return 0;
    }

    @Override
    public int qualityUpdateBeforeSellIn() {
        return 0;
    }

    @Override
    public int qualityUpdateAfterSellDate() {
        return 0;
    }
}

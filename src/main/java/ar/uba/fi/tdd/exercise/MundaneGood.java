package ar.uba.fi.tdd.exercise;

public abstract class MundaneGood extends Good {
    public static final int SELL_IN_DAILY_UPDATE = 1;
    public static final int MAX_QUALITY = 50;
    public static final int NORMAL_FACTOR = 1;
    public static final int AFTER_SELL_IN_MULTIPLIER = 2;

    public MundaneGood(String name, int sellIn, int quality) {
        super(name, sellIn, quality);

        if (quality > MAX_QUALITY) {
            throw new InvalidQualityException();
        }
    }

    @Override
    public int sellInUpdate() {
        return - SELL_IN_DAILY_UPDATE;
    }
}

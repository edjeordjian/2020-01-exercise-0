package ar.uba.fi.tdd.exercise;

public class Conjured extends MundaneGood {
    public static final int CONJURED_MULTIPLIER = 2;

    public Conjured(int sellIn, int quality) {
        super("Conjured", sellIn, quality);
    }

    @Override
    public int qualityUpdateBeforeSellIn() {
        return - CONJURED_MULTIPLIER * NORMAL_FACTOR;
    }

    @Override
    public int qualityUpdateAfterSellDate() {
        return - AFTER_SELL_IN_MULTIPLIER * CONJURED_MULTIPLIER * NORMAL_FACTOR;
    }
}

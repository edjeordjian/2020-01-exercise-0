package ar.uba.fi.tdd.exercise;

public abstract class Good {
    public static final int MIN_SELL_IN = 0;
    public static final int MIN_QUALITY = 0;
    private int sellIn;
    private int quality;
    private String name;

    public Good(String name, int sellIn, int quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;

        if (quality < MIN_QUALITY) {
            throw new InvalidQualityException();
        }
    }

    public int getSellIn() {
        return this.sellIn;
    }

    public void setSellIin(int sellIn) {
        this.sellIn = StrictMath.max(sellIn , MIN_SELL_IN);
    }

    public int getQuality() {
        return this.quality;
    }

    public void setQuality(int quality) {
        this.quality = StrictMath.max(quality , MIN_QUALITY);
    }

    public boolean sellInIsMin() {
        return this.sellIn == MIN_SELL_IN;
    }

    public void update() {
        this.setSellIin( this.getSellIn() + sellInUpdate() );
        this.setQuality( this.getQuality() +
            ( sellInIsMin() ?
            qualityUpdateAfterSellDate() :
            qualityUpdateBeforeSellIn() ) );
    }

    public abstract int sellInUpdate();

    public abstract int qualityUpdateBeforeSellIn();

    public abstract int qualityUpdateAfterSellDate();

    @Override
    public boolean equals(Object o) {
        Good g;

        try {
            g = ( (Good) o );
        }

        catch (ClassCastException e) {
            return false;
        }

        return this.toString().equals( g.toString() );
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public String toString() {
        return name;
    }
}
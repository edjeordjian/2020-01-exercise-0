package ar.uba.fi.tdd.exercise;

public class AgedBrie extends MundaneGood {
    public AgedBrie(int sellIn, int quality) {
        super("Aged Brie", sellIn, quality);
    }

    public AgedBrie(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public int qualityUpdateBeforeSellIn() {
        return StrictMath.min(NORMAL_FACTOR,
            MAX_QUALITY - this.getQuality());
    }

    @Override
    public int qualityUpdateAfterSellDate() {
        return StrictMath.min(
            AFTER_SELL_IN_MULTIPLIER * NORMAL_FACTOR,
            MAX_QUALITY - this.getQuality() );
    }
}

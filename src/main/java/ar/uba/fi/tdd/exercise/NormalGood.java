package ar.uba.fi.tdd.exercise;

public class NormalGood extends MundaneGood {
    public NormalGood(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public int qualityUpdateBeforeSellIn() {
        return - NORMAL_FACTOR;
    }

    @Override
    public int qualityUpdateAfterSellDate() {
        return - AFTER_SELL_IN_MULTIPLIER * NORMAL_FACTOR;
    }
}

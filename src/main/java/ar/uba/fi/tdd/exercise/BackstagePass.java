package ar.uba.fi.tdd.exercise;

import java.util.*;

public class BackstagePass extends AgedBrie {
    private Map<Integer, Integer> qualityIncreaseAccordingToDaysLeft;

    public BackstagePass(int sellIn, int quality) {
        super("Backstage Pass", sellIn, quality);

        if (this.sellInIsMin() &&  this.getQuality() != MIN_QUALITY) {
            throw new InvalidQualityException();
        }

        qualityIncreaseAccordingToDaysLeft = new
            LinkedHashMap<Integer, Integer>();
        qualityIncreaseAccordingToDaysLeft.put(5, 3);
        qualityIncreaseAccordingToDaysLeft.put(10, 2);
    }

    @Override
    public int qualityUpdateBeforeSellIn() {
        for (Integer daysAmount :
                qualityIncreaseAccordingToDaysLeft.keySet() ) {
            if ( this.getSellIn() <= daysAmount ) {
                return qualityIncreaseAccordingToDaysLeft.get( daysAmount);
            }
        }

        return super.qualityUpdateBeforeSellIn();
    }

    @Override
    public int qualityUpdateAfterSellDate() {
        return - this.getQuality();
    }
}
